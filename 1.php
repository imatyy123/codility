<?php

/*
 * Write a function that, given two strings S and T consisting of N and M characters, respectively,
 * determines whether string T can be obtained from string S by at most one insertion or deletion
 * of a character, or by swapping two adjacent charcters once. The function should return a string:
 * - "INSERT c" if string T can be obtained from string S by inserting a single character "c";
 * - "DELETE c" if string T can be obtained from string S by deleting a single character "c";
 * - "SWAP c d" if string T can be obtained from String S by swapping two adjacent characters "c" 
 *    and "d" (these characters should be distinct and they should be in this order in string S);
 * - "NOTHING" if no operation is needed (strings T and S are equal);
 * - "IMPOSSIBLE" if none of the above works.   
 */


function solution($S, $T)
{
    $s_array = str_split($S);
    $t_array = str_split($T);
    array_walk($s_array,'convert_char_to_ascii');
    array_walk($t_array,'convert_char_to_ascii');   
    if($S == $T)
    {
        return "NOTHING";
    }
    elseif(strlen($S) - strlen($T) > 1 || strlen($T) - strlen($S) > 1)
    {
        return "IMPOSSIBLE";
    }
    else
    {
        if(strlen($S) > strlen($T))
        {
            return "DELETE ".get_missing_char($s_array,$t_array);
        }
        elseif(strlen($T) > strlen($S))
        {
            return "INSERT ".get_missing_char($t_array,$s_array);
        }
        else
        {
            $swap_chars = array_diff_assoc($s_array,$t_array);
            array_walk($swap_chars,'convert_ascii_to_char');
            return "SWAP ".implode(" ",$swap_chars);
        }
    }
}

function convert_char_to_ascii(&$value)
{
     $value = ord($value);
}

function convert_ascii_to_char(&$value)
{
     $value = chr($value);
}

function get_missing_char($a,$b)
{
    $ascii = array_sum($a) - array_sum($b);
    return chr($ascii);
}


?>