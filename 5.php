<?php
/*
 * A rectangle is called rectilinear if its edges are all parallel to coordinate axes. 
 * Such a rectangke can be described by specifying the coordinates of its lower-left
 * and upper-right corners:
 * Write a function that, given eight integers representing two rectilinear rectangles
 * (one with lower-left corner (K,L) and upper-right corner (M,N) and another with 
 * lower-left corner (P,Q) and upper-right corner (R,S)), returns the area of the sum of
 * the rectangles. If the rectangles intersect, the area of their intersection should be
 * counted only once. The function should return -1 if the area of the sum exceeds 
 * 2,147,483,647.
 * For example, given integers:
 * K = -4    L = 1    M = 2    N = 6
 * P = 0     Q = -1   R = 4    S = 3
 * the function should return 42.
 * - expected worst-case time complexity is O(1);
 * - expected worst-case space complexity is O(1). 
 */

function solution($K, $L, $M, $N, $P, $Q, $R, $S)
{
    $A1 = ($M-$K) * ($N-$L);
    $A2 = ($R-$P) * ($S-$Q);
    $ox = max(0,min($M,$R)-max($K,$P));
    $oy = max(0,min($N,$S)-max($L,$Q));  
    $A3 = $ox * $oy;
    $area = ($A1 + $A2) - $A3;
    return ($area > 2147483647) ? -1 : $area;           
} 

?>